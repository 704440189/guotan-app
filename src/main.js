import Vue from 'vue'
import 'babel-polyfill'
import App from './App.vue'
import router from './router'
import store from './store'
import Vant from 'vant'
import 'vant/lib/index.css'
import area from '@/modules/area'
import http from '@/modules/http'
import storage from '@/modules/storage'
import lottery from 'vue-lottery'
import moment from 'moment'
import axios from 'axios'



Vue.use(Vant)
Vue.use(lottery)



Vue.config.productionTip = false
Vue.prototype.$http=http
Vue.prototype.storage=storage
Vue.prototype.area=area
Vue.prototype.moment=moment
Vue.prototype.axios=axios
Vue.prototype.imgUrl='http://neufit2.src.trycheers.com/'//图片资源域名
Vue.prototype.hrefUrl='http://neufit2.shop.trycheers.com/'//前端项目域名---支付
//Vue.prototype.hrefUrl='http://wap.neufit.cc/'//前端项目域名---支付

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
