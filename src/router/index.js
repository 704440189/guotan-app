import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'
import storage from '@/modules/storage'
import store from '../store'
import { Dialog } from 'vant'

Vue.use(Router)

const router = new Router({
  routes: routes,
  mode: 'hash'
})
router.beforeEach((to, from, next) => {
  let session=storage.get('session') || false
    if(session==false){
      if(to.name!='login'&&to.name!='regist'){
        next('/login')
      }else{
        next()
      }
    }else{
      next()
    }
})
export default router