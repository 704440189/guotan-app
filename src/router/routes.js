const Home = () => import('@/pages/Home')
const Mall = () => import('@/pages/Mall')
const Search = () => import('@/pages/Search')
const Cart = () => import('@/pages/Cart')


//const CateGory = () => import('@/pages/CateGory')
const GoodsList = () => import('@/pages/GoodsList')
const Detail = () => import('@/pages/Detail')
const ManageAddress = () => import('@/pages/ManageAddress')
const AddressList = () => import('@/pages/AddressList')
const EditAddress = () => import('@/pages/EditAddress')

const ComfirmOrder = () => import('@/pages/ComfirmOrder')
const PaySuccess = () => import('@/pages/PaySuccess')
const Login = () => import('@/pages/Login')
const Regist = () => import('@/pages/Regist')
const Kill = () => import('@/pages/Kill')
const KillDetail = () => import('@/pages/KillDetail')

const Mine = () => import('@/pages/Mine')
const EditUser = () => import('@/pages/mine/EditUser')
const ChangePwd = () => import('@/pages/mine/ChangePwd')
const Abount = () => import('@/pages/mine/Abount')
const Newer = () => import('@/pages/mine/Newer')
const MyOrder = () => import('@/pages/mine/MyOrder')
const AfterSev = () => import('@/pages/mine/AfterSev')
const MyCollect = () => import('@/pages/mine/MyCollect')
const OrderDetail = () => import('@/pages/mine/OrderDetail')
const MyBag = () => import('@/pages/mine/MyBag')
const Withdrawal = () => import('@/pages/mine/Withdrawal')
const WithdrawalSuccess = () => import('@/pages/mine/WithdrawalSuccess')
const MyTicket = () => import('@/pages/mine/MyTicket')
const TicketBag = () => import('@/pages/mine/TicketBag')
const RefundDetail = () => import('@/pages/mine/RefundDetail')
const ApplyRefund = () => import('@/pages/mine/ApplyRefund')
const WinRecord = () => import('@/pages/mine/WinRecord')

const Empty = () => import('@/pages/Empty')
const Game = () => import('@/pages/Game')
const Scan = () => import('@/pages/Scan')



const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/scan',
    name:'scan',
    component: Scan
  },
  {
    path: '/game',
    name:'game',
    component: Game
  },
  {
    path: '*',
    name:'empty',
    component: Empty
  },
  {
    path: '/login',
    name:'login',
    component: Login,
    meta:{haveLogin:false}
  },
  {
    path: '/regist',
    name:'regist',
    component: Regist
  },
  {
    path: '/home',
    name:'home',
    component: Home,
    meta:{isnav:true}
  },
  {
    path: '/kill',
    name:'kill',
    component: Kill
  },
  {
    path: '/killDetail',
    name:'killDetail',
    component: KillDetail
  },
  {
    path: '/goodsList',
    name:'goodsList',
    component: GoodsList
  },
  {
    path: '/detail',
    name:'detail',
    component: Detail
  },
  {
    path: '/comfirmOrder',
    name:'comfirmOrder',
    component: ComfirmOrder
  },
  {
    path: '/paySuccess',
    name:'paySuccess',
    component: PaySuccess
  },
  {
    path: '/manageAddress',
    name:'manageAddress',
    component: ManageAddress
  },
  {
    path: '/addressList',
    name:'addressList',
    component: AddressList
  },
  {
    path: '/editAddress',
    name:'editAddress',
    component: EditAddress
  },
  {
    path: '/search',
    name:'search',
    component: Search
  },
  {
    path: '/mall',
    name:'mall',
    component: Mall,
    meta:{isnav:true}
  },
  {
    path: '/cart',
    name:'cart',
    component: Cart,
    meta:{isnav:true}
  },
  {
    path: '/mine',
    name: 'mine',
    component:Mine,
    meta: { authRequired: true,isnav:true},
  },
  {
    path: '/editUser',
    name: 'editUser',
    component:EditUser
  } ,
  {
    path: '/changePwd',
    name: 'changePwd',
    component:ChangePwd
  } ,
  {
    path: '/abount',
    name: 'abount',
    component:Abount
  } ,
  {
    path: '/newer',
    name: 'newer',
    component:Newer
  } ,
  {
    path: '/myOrder',
    name: 'myOrder',
    component:MyOrder
  } ,
  {
    path: '/afterSev',
    name: 'afterSev',
    component:AfterSev
  } ,
  {
    path: '/myCollect',
    name: 'myCollect',
    component:MyCollect
  },
  {
    path: '/orderDetail',
    name: 'orderDetail',
    component:OrderDetail
  } ,
  {
    path: '/refundDetail',
    name: 'refundDetail',
    component:RefundDetail
  } ,
  {
    path: '/applyRefund',
    name: 'applyRefund',
    component:ApplyRefund
  } ,
  {
    path: '/myBag',
    name: 'myBag',
    component:MyBag
  },
  {
    path: '/withdrawal',
    name: 'withdrawal',
    component:Withdrawal
  },
  {
    path: '/withdrawalSuccess',
    name: 'withdrawalSuccess',
    component:WithdrawalSuccess
  },
  {
    path: '/myTicket',
    name: 'myTicket',
    component:MyTicket
  }, 
  {
    path: '/ticketBag',
    name: 'ticketBag',
    component:TicketBag
  }, 
  {
    path: '/winRecord',
    name: 'winRecord',
    component:WinRecord
  }, 

]
export default routes
