var sWeek = new Array("周日", "周一", "周二", "周三", "周四", "周五", "周六");
	var dNow = new Date();
	var CalendarData = new Array(100);
	var madd = new Array(12);
	var tgString = "甲乙丙丁戊己庚辛壬癸";
	var dzString = "子丑寅卯辰巳午未申酉戌亥";
	var numString = "一二三四五六七八九十";
	var monString = "正二三四五六七八九十冬腊";
	var sx = "鼠牛虎兔龙蛇马羊猴鸡狗猪";
	var cYear, cMonth, cDay, TheDate;
	CalendarData = new Array(0xA4B, 0x5164B, 0x6A5, 0x6D4, 0x415B5, 0x2B6, 0x957, 0x2092F, 0x497, 0x60C96, 0xD4A, 0xEA5, 0x50DA9, 0x5AD, 0x2B6, 0x3126E, 0x92E, 0x7192D, 0xC95, 0xD4A, 0x61B4A, 0xB55, 0x56A, 0x4155B, 0x25D, 0x92D, 0x2192B, 0xA95, 0x71695, 0x6CA, 0xB55, 0x50AB5, 0x4DA, 0xA5B, 0x30A57, 0x52B, 0x8152A, 0xE95, 0x6AA, 0x615AA, 0xAB5, 0x4B6, 0x414AE, 0xA57, 0x526, 0x31D26, 0xD95, 0x70B55, 0x56A, 0x96D, 0x5095D, 0x4AD, 0xA4D, 0x41A4D, 0xD25, 0x81AA5, 0xB54, 0xB6A, 0x612DA, 0x95B, 0x49B, 0x41497, 0xA4B, 0xA164B, 0x6A5, 0x6D4, 0x615B4, 0xAB6, 0x957, 0x5092F, 0x497, 0x64B, 0x30D4A, 0xEA5, 0x80D65, 0x5AC, 0xAB6, 0x5126D, 0x92E, 0xC96, 0x41A95, 0xD4A, 0xDA5, 0x20B55, 0x56A, 0x7155B, 0x25D, 0x92D, 0x5192B, 0xA95, 0xB4A, 0x416AA, 0xAD5, 0x90AB5, 0x4BA, 0xA5B, 0x60A57, 0x52B, 0xA93, 0x40E95);
	madd[0] = 0;
	madd[1] = 31;
	madd[2] = 59;
	madd[3] = 90;
	madd[4] = 120;
	madd[5] = 151;
	madd[6] = 181;
	madd[7] = 212;
	madd[8] = 243;
	madd[9] = 273;
	madd[10] = 304;
	madd[11] = 334;

	function GetBit(m, n) {
		return(m >> n) & 1;
	}

	function e2c() {
		TheDate = (arguments.length != 3) ? new Date() : new Date(arguments[0], arguments[1], arguments[2]);
		var total, m, n, k;
		var isEnd = false;
		var tmp = TheDate.getFullYear();
		total = (tmp - 1921) * 365 + Math.floor((tmp - 1921) / 4) + madd[TheDate.getMonth()] + TheDate.getDate() - 38;
		if(TheDate.getYear() % 4 == 0 && TheDate.getMonth() > 1) {
			total++;
		}
		for(m = 0;; m++) {
			k = (CalendarData[m] < 0xfff) ? 11 : 12;
			for(n = k; n >= 0; n--) {
				if(total <= 29 + GetBit(CalendarData[m], n)) {
					isEnd = true;
					break;
				}
				total = total - 29 - GetBit(CalendarData[m], n);
			}
			if(isEnd) break;
		}
		cYear = 1921 + m;
		cMonth = k - n + 1;
		cDay = total;
		if(k == 12) {
			if(cMonth == Math.floor(CalendarData[m] / 0x10000) + 1) {
				cMonth = 1 - cMonth;
			}
			if(cMonth > Math.floor(CalendarData[m] / 0x10000) + 1) {
				cMonth--;
			}
		}
	}

	/*戊戌狗年 腊月廿*/
	function GetcDateString() {
		let year1 = "";
        year1 += tgString.charAt((cYear - 4) % 10);
        year1 += dzString.charAt((cYear - 4) % 12);
        year1 += "年";
        year1=year1.split('')
        let year2=''
		year2 += sx.charAt((cYear - 4) % 12);
        year2 += "年";
        year2=year2.split('')

        let month='农历'
		if(cMonth < 1) {
			month += "(闰)";
			month += monString.charAt(-cMonth - 1);
		} else {
			month += monString.charAt(cMonth - 1);
		}
        month += "月";
		month += (cDay < 11) ? "初" : ((cDay < 20) ? "十" : ((cDay < 30) ? "廿" : "三十"));
		if(cDay % 10 != 0 || cDay == 10) {
			month += numString.charAt((cDay - 1) % 10);
        }
        month=month.split('')
		return {year1,year2,month};
	}

	function GetLunarDay(solarYear, solarMonth, solarDay) {
		if(solarYear < 1921 || solarYear > 2020) {
			return "";
		} else {
			solarMonth = (parseInt(solarMonth) > 0) ? (solarMonth - 1) : 11;
			e2c(solarYear, solarMonth, solarDay);
			return GetcDateString();
		}
	}
	function getFullYear(d) { // 修正firefox下year错误
		let yr = d.getYear();
		if(yr < 1000) yr += 1900;
		return yr;
    }
    function dealNum(str,flag){
        if(flag=='y'){
            str=String(str)
            str=str.split('')
        }
        else{
            str=[str]
        }
        return str.map(i=>{
            switch(Number(i)){
                case 0:
                   i='零'
                   break;
                case 1:
                   i='一'
                   break;
                case 2:
                   i='二'
                   break;
                case 3:
                   i='三'
                   break;
                case 4:
                   i='四'
                   break;
                case 5:
                   i='五'
                   break;
                case 6:
                   i='六'
                   break;
                case 7:
                   i='七'
                   break;
                case 8:
                   i='八'
                   break;
                case 9:
                   i='九'
                   break;
                case 10:
                   i='十'
                   break;
                case 11:
                   i='十一'
                   break;
                case 12:
                   i='十二'
                   break;
            }
            return i
        })
        
    }
        // time
        var D = new Date();
        var time={}
        var year=getFullYear(dNow)
        year=dealNum(year,'y')
        var month=dNow.getMonth() + 1
        month=dealNum(month,'m').join('')+'月'
        month=month.split('')
        var day=dNow.getDate()
        var week=sWeek[dNow.getDay()]
        week=week.split('')
        var yy = D.getFullYear();
		var mm = D.getMonth() + 1;
		var dd = D.getDate();
        var lunar = GetLunarDay(yy, mm, dd);
        time={year,month,day,week,lunar}
export default {
    time
}