import request from '@/modules/request'
import storage from '@/modules/storage'
import axios from 'axios'
import qs from 'qs'
import {Dialog } from 'vant'

async function http(params,flag){
    let result=null
    if(params.method=='get'){
         result = await request.get(params.url,qs.stringify(params.data))
    }
    if(params.method=='gets'){
      result = await request.get(params.url,params.data)
 }
    if(params.method=='post'){
         result = await request.post(params.url, qs.stringify(params.data))
    }
    if(params.method=='posts'){
          result = await request.post(params.url, JSON.stringify(params.data))
    }
    if(params.method=='upload'){
          result = await request.post(params.url, params.data)
    }
    if(params.method=='put'){
         result = await request.put(params.url,qs.stringify(params.data))
    }
    if(params.method=='puts'){
          result = await request.put(params.url,JSON.stringify(params.data))
    }
    if(params.method=='delete'){
        result = await request.delete(params.url,{data:qs.stringify(params.data)})
    }
    if(result.status===403 && !flag){// 403:token过期;重定向到登陆页面
        let session=storage.get('session')
        let formData={mobile:session.mobile,password:session.password,openid:session.openid}
        try {
          const res = await axios.post(`${process.env.API_BASE_URL}/user_login`, qs.stringify(formData))
          if(res.data.status===1){
              let session=storage.get('session')
              Object.assign(session,res.data.data)
              storage.set('session',session)
              return http(params,true)
          }else{
             window.location.href='/login'
          }
        } catch (error) {
          if (error.response && error.response.data) {
            throw new Error(error.response.data.message)
          } else {
            throw error
          }
        }
    }else{
      return result
    }
}

export default http
