import axios from 'axios'
import qs from 'qs'
import storage from '@/modules/storage'
import router from '../router/'
import { Toast, Dialog } from 'vant'
const $http = axios.create({
  baseURL: process.env.API_BASE_URL//'http://neufit2.trycheers.com'//
})
// 请求拦截
$http.interceptors.request.use((config) => {
  Toast.loading({
    mask: true,
    message: '加载中...'
  })
  let token = storage.get('session').token
  config.headers['token']=token
  // config.headers['Content-Type']='application/x-www-form-urlencoded'
  return config
})
//响应拦截
$http.interceptors.response.use((res) => {
  Toast.clear()
  handleSuccessRes(res.data)
  return res.data
}, async (error) => {
  await handleResponseError(error)
  return false
})
/**处理请求错误后的错误处理 */
async function handleResponseError (error) {
  if (error.response && error.response.status === 400) {
    await Dialog.alert({
      title: '错误提示',
      message: error.response.data.message
    })
  } else if (error.response && error.response.status === 404) {
    await Dialog.alert({
      title: '错误提示',
      message: `请求失败，请稍后重试`
    })
  } else if (error.response.data && error.response.data.message) {
    await Dialog.alert({
      title: '错误提示',
      message: error.response.data.message
    })
  } else {
    await Dialog.alert({
      title: '错误提示',
      message: `服务器遇到了一些问题，请稍后再试。${error.response ? '错误码：' + error.response.status : ''}`
    })
  }
}
/**处理请求成功后的错误处理 */
async function handleSuccessRes (data) {
  if (data.status === 0) { //0:请求错误
    await Dialog.alert({
      title: '错误提示',
      message: `${data.message}`
    })
  }
}
export default $http
