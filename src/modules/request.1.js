import axios from 'axios'
import qs from 'qs'
import storage from '@/modules/storage'
import router from '../router/'
import { Toast, Dialog } from 'vant'
const $http = axios.create({
  baseURL: process.env.API_BASE_URL//'http://neufit2.trycheers.com'//
})
// 请求拦截
$http.interceptors.request.use((config) => {
  Toast.loading({
    mask: true,
    message: '加载中...'
  })
  let token = storage.get('session').token
  if(config.method=='post'){
      let data = qs.parse(config.data)
      config.data = qs.stringify({
            token: token,
            ...data
      })
  }else if(config.method=='get'){
      config.params = {
          token,
          ...config.params
      }
  }
  return config // config是ajax的配置文件，如果不返回，则ajax则不能请求,的不到数据
})
//响应拦截
$http.interceptors.response.use((res) => {
  Toast.clear()
  handleSuccessRes(res.data)
  return res.data
}, async (error) => {
  await handleResponseError(error)
  return false
})

function getToken() {//获取token
 // console.log(storage.get('session').token)
  return storage.get('session').token
}

async function handleSuccessRes (data) {//处理请求成功后的错误处理
  if (data.status === 0) { //0:请求错误
    await Dialog.alert({
      title: '错误提示',
      message: `${data.message}`
    })
  }
  else if(data.status===403){// 403:token过期;重定向到登陆页面
    await Dialog.alert({
      title: '错误提示',
      message: `${data.message}`
    })
    storage.clear()//清空本地
		router.replace({
      path: '/login',
      query: {redirect: router.currentRoute.fullPath}
    })

  }
}

async function handleResponseError (error) {
  if (error.response && error.response.status === 400) {
    await Dialog.alert({
      title: '错误提示',
      message: error.response.data.message
    })
  } else if (error.response && error.response.status === 401) {
  } else if (error.response.data && error.response.data.message) {
    await Dialog.alert({
      title: '错误提示',
      message: error.response.data.message
    })
  } else {
    await Dialog.alert({
      title: '错误提示',
      message: `服务器遇到了一些问题，请稍后再试。${error.response ? '错误码：' + error.response.status : ''}`
    })
  }
}
export default $http
