
const prefix = 'neufit/'

function keyWithPrefix(key) {
  return prefix + key
}

function parse(value) {
  try {
    return JSON.parse(value).value
  } catch (e) {
    return null
  }
}

function stringify(value) {
  return JSON.stringify({
    value: value
  })
}

export default {
  get(key) {
    return parse(localStorage.getItem(keyWithPrefix(key)))
  },

  set(key, value) {
    return localStorage.setItem(keyWithPrefix(key), stringify(value))
  },

  remove(key) {
    return localStorage.removeItem(keyWithPrefix(key))
  },

  clear() {
    localStorage.clear()
  }
}
