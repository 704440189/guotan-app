import http from '@/modules/http'
import moment from 'moment'
import axios from 'axios'
import storage from '@/modules/storage'
const state = {
    redBag:{},//红包
    quanBag:{},
    packageList:[],//红包
    weekBagList:[],
    monthBagList:[],
    voucherList:[],//牛宝券
    weekQuanList:[],
    monthQuanList:[],
    isEmpty:false

}
function filterArr(list,flag){
    console.log(list)
    if(flag==0){
        var week=moment().subtract(7, 'days').format('YYYY-MM-DD')
    }else{
        var week=moment().subtract(1, 'months').format('YYYY-MM-DD')
    }
    list=list.filter(item=>{
        let time=moment(item.create_time).format('YYYY-MM-DD')
        console.log(time,moment(week).isBefore(time))
        if(moment(week).isBefore(time)){
            return item
        } 
    })
    console.log(list)
   return list
}
const mutations = {
    setRedBag(state,data){
        state.redBag=data
    },
    setQuanBag(state,data){
        state.quanBag=data
    },
    setBaglist(state,data){
        data=data.map(item=>{ 
            let time=Number(item.create_time)
            item.create_time=moment(time).format('YYYY年MM月DD日')
            return item
        })
        state.packageList=data
        state.weekBagList=filterArr(data,0)
        console.log(state.weekBagList)
    },
    setQuanlist(state,data){
        data=data.map(item=>{ 
            let time=item.create_time
            item.create_time=moment(time).format('YYYY年MM月DD日')
            return item
        })
        state.voucherList=data
        state.weekQuanList=filterArr(data,0)
    }
}
const actions = {
    getRedBag({ commit }) {//加载我的红包--1.收入，-1.支出，0.余额。不传返回总量
        let obj1={method:'get',url:`package_summary?type=0`}
        let obj2={method:'get',url:'package_summary?type=1'}
        let obj3={method:'get',url:'package_summary?type=-1'}
        let obj={}
        axios.all([
            http(obj1),
            http(obj2),
            http(obj3)
          ]).then(axios.spread(function (res1, res2,res3) {
            obj={balance:res1.data.summary,income:res2.data.summary,outcome:res3.data.summary}
            commit('setRedBag',obj)
          }));
        return obj
    },
    async getBagList({ commit },params) {//卡包操作历史记录
        let obj={method:'post',url:'package_list',data:params}
        const result = await http(obj)
        // if (result) commit('setBaglist', result.data)
        return result
    },
    async getquanList({ commit },params) {//卡包操作历史记录
        let obj={method:'post',url:'voucher_list',data:params}
        const result = await http(obj)
        // if (result) commit('setQuanlist', result.data)
        return result
    },
    async paymentBag() {//红包体现
        let obj={method:'get',url:'payment'}
        const result = await http(obj)
        return result
    },
    getVoucher({ commit }) {//加载牛宝券
        let obj1={method:'get',url:'voucher_summary?type=0'}
        let obj2={method:'get',url:'voucher_summary?type=1'}
        let obj3={method:'get',url:'voucher_summary?type=-1'}
        let obj={}
        axios.all([
            http(obj1),
            http(obj2),
            http(obj3)
          ]).then(axios.spread(function (res1, res2,res3) {
            obj={balance:res1.data.summary,income:res2.data.summary,outcome:res3.data.summary}
            commit('setQuanBag',obj)
          }))
        return obj
    },
}
  
export default {
    namespaced: true,
    state,
    mutations,
    actions
}
  