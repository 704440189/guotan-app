import request from '@/modules/request'
import storage from '@/modules/storage'
import axios from 'axios'
import qs from 'qs'
import {Dialog } from 'vant'
const state = {
    cartList:storage.get('cart')||[],
    allchioce:false,
  }
const mutations = {
    addCart(state,data){//加入购物车
        let isfind=false
        let isKill=false
        state.cartList.map(item=>{
            if(item.id==data.id){
                isfind=true
            }
            if(item.status){
                isKill=true
            }
            return item
        })
        if(isfind==false){
            state.cartList.push(data)
        }else{
            if(!isKill){
                state.cartList.map(item=>{
                    item.count++
                    return item
                }) 
            }
            
        }
    },
    deleteCart(state,id){//删除购物车
        if(id==0){
            state.cartList=state.cartList.filter(item=>item.ischeck==false)
        }
        else{
            state.cartList=state.cartList.filter(item=>item.id!=id)
        }
    },
    changeCount(state,data){//改变数量
        state.cartList.map(item=>{
            if(item.id==data.id){
                item.count=data.count
            }
        })
    },
    setAllCheck(state){//全选
        state.allchioce=!state.allchioce
        state.cartList=state.cartList.map(item=>{
            item.ischeck=state.allchioce
            return item
        })
    },
    setAllxheckDefalut(state){
       state.allchioce=false
    },
    setRadio(state,id){//单选
        state.cartList=state.cartList.map(item=>{
            if(item.id==id){
               item.ischeck=!item.ischeck
            }
            return item
        })
        let flag=state.cartList.find(item=>item.ischeck==false)
        if(flag==undefined){
            state.allchioce=true
        }else{
            state.allchioce=false
        }
    }
}
const actions = {
}
  
export default {
    namespaced: true,
    state,
    mutations,
    actions
}
  