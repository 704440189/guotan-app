
import http from '@/modules/http'
import storage from '@/modules/storage'
import axios from 'axios'
import qs from 'qs'
import {Dialog } from 'vant'
const state = {
    addressList:[],//地址列表
    defaultAddress:{}//默认地址
  }
const mutations = {
    getList(state,data){
        let list=data.map(item=>{
            item.default=Number(item.default)
            return item
        })
        state.addressList=list.reverse()
    },
    getDefault(state,data){
        state.defaultAddress=data
    },
    setDefault(state,data){
        let list=state.addressList
        state.addressList=list.map(item=>{
            item.default=0
            if(item.id==data.id){
                item.default=1
            }
            return item
        })
    },
    delAddress(state,data){
        state.addressList=state.addressList.filter(item=>item.id!=data.id)
    }
}
const actions = {
    async loadAddress({ commit ,dispatch}) {//加载收货地址
        let obj={method:'get',url:'address_list'}
        const result =await http(obj)
        if (result) commit('getList',result.data)
        return result
    },
    async setDefaultAddress({ commit },params) {//设置默认收货地址
        let obj={method:'put',url:'address_select',data:params}
        const result = await http(obj)
        if(result) commit('setDefault',params)
        return result
    },
    async loadDefaultAddress({ commit }) {//获取默认收货地址
        let obj={method:'get',url:'address_default'}
        const result = await http(obj)
        if (result) commit('getDefault',result.data)
        return result
    },
    async addAddress({ dispatch }, params) {//增加收货地址
        let obj={method:'put',url:'address_add',data:params}
        const result = await http(obj)
        return result
    },
    async updateAddress({ commit }, params) {//编辑收货地址
        let obj={method:'put',url:'address_edit',data:params}
        const result = await http(obj)
        return result
    },
    async deleteAddress({ commit }, params) {//删除收货地址
        let obj={method:'delete',url:'address_del',data:params}
        const result = await http(obj)
        if(result) commit('delAddress',params)
        return result
    },
}
  
export default {
    namespaced: true,
    state,
    mutations,
    actions
}
  