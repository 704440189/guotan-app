
import http from '@/modules/http'
import storage from '@/modules/storage'
import qs from 'qs'
const state = {
    comfirmOrder:storage.get('comfirmOrder')||{type:0,list:[]},//商品确认列表
    orderInfo:{},//创建订单成功后返回数据
    orderKillInfo:{},//秒杀创建订单成功后返回数据
    orderList:[],//订单列表
    orderTotal:0,
    
}
const mutations = {
    setOredrInfo(state,data){
        state.orderInfo=data
    },
    setOrderList(state,data){
        state.orderList=data
    },
    setOrderTotal(state,data){
        state.orderTotal=data.ordertotal
    },
    setComfirmOrderList(state,data){
        state.comfirmOrder.list=data.list
        state.comfirmOrder.type=data.type
    }
}

let paydata=null
const actions = {
    async orderCreate({ commit},params) {//普通订单下单
        let obj={method:'puts',url:'order_create',data:params}
        const result=await http(obj)
        if (result){
            storage.set('currentOrder',result.data)
            commit('setOredrInfo',result.data)
        }
        return result  
    },
    async orderKillCreate({ commit},params) {//秒杀订单下单
        let obj={method:'post',url:'seckill_create',data:params}
        const result = await http(obj)
        if (result){
            storage.set('currentOrder',result.data)
            commit('setOredrInfo',result.data)
        }
        return result 
    },
    async weixinPay({ commit },params) {//微信支付
        let obj={method:'post',url:'pay',data:params}
        const result=await http(obj)
        if(result &&result.status==1){
            paydata=result.data
            callpay()
        }
        return result
    },
    async cancelOrder({ commit },params) {//取消订单
        let obj={method:'puts',url:'order_cancel',data:params}
        const result = await http(obj)
        return result
    },
    async applyOutMoney({ dispatch }, params) {//申请退款
        let obj={method:'post',url:'apply_out_money',data:params}
        const result = await http(obj)
        return result
    },
    async undoApplyOut({ dispatch }, params) {//撤消申请退款order_id
        let obj={method:'post',url:'undo_apply_out_money',data:params}
        const result = await http(obj)
        return result
    },
    async applyOutGoods({ dispatch }, params) {//申请退货order_id
        let obj={method:'post',url:'apply_out_goods',data:params}
        const result = await http(obj)
        return result
    },
    async undoApplyOutGoods({ dispatch }, params) {//撤消申请退货order_id
        //const result = await request.post('/undo_apply_out_goods', qs.stringify(id))
        let obj={method:'post',url:'undo_apply_out_goods',data:params}
        const result = await http(obj)
        return result
    },
    async comfirmGoods({ commit },params) {//确认收货
        let obj={method:'post',url:'order_confirm',data:params}
        const result = await http(obj)
        return result
    },
    async getOrderList({ commit },params) {//订单列表
        let obj={method:'post',url:'/order',data:params}
        const result = await http(obj)
        return result
    },
    async getOrderTotal({ commit }) {//订单总数
        let obj={method:'get',url:'order_total'}
        const result = await http(obj) 
        return result
    },
    async deleteOrder({ dispatch }, params) {//删除订单
        let obj={method:'delete',url:'order_del',data:params}
        const result = await http(obj)
        return result
    },
    async orderDetail({ dispatch }, params) {//订单详情
        let obj={method:'post',url:'order_detail',data:params}
        const result = await http(obj)
        //const result = await request.post(`/order_detail`,qs.stringify(id))
        return result
    },
    async getPrizeList({ commit },params) {//获奖列表
        let obj={method:'post',url:'prize_user_list',data:params}
        const result = await http(obj)
        return result
    },
}

function jsApiCall(){
    const data=paydata
    console.log(data,paydata)
    WeixinJSBridge.invoke(//调用微信支付
        'getBrandWCPayRequest',
        data,
        function (res) {
            WeixinJSBridge.log(res.err_msg);
            //alert(res.err_code + res.err_desc + res.err_msg);
        }
    )
}
function callpay(){
    if (typeof WeixinJSBridge == "undefined") {
        if (document.addEventListener) {
            document.addEventListener('WeixinJSBridgeReady',jsApiCall, false);
        } else if (document.attachEvent) {
            document.attachEvent('WeixinJSBridgeReady', jsApiCall);
            document.attachEvent('onWeixinJSBridgeReady',jsApiCall);
        }
    } else {
        jsApiCall()
    }
}
  
export default {
    namespaced: true,
    state,
    mutations,
    actions
}
  