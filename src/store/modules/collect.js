import http from '@/modules/http'
import storage from '@/modules/storage'
import axios from 'axios'
import qs from 'qs'
import {Dialog } from 'vant'
const state = {
    collectList:[],
    totalNum:0,
    allchioce:false,
  }
const mutations = {
    setList(state,data){
        let list=data
        list=list.map(item=>{
            item.ischeck=false
            return item
        })
        state.collectList=list
    },
    radioSelect(state,id){
        state.collectList=state.collectList.map(item=>{
            if(item.id==id){
               item.ischeck=!item.ischeck
            }
            return item
        })
        let flag=state.collectList.find(item=>item.ischeck==false)
        if(flag==undefined){
            state.allchioce=true
        }else{
            state.allchioce=false
        }
    },
    allSelect(state){
        state.allchioce=!state.allchioce
        state.collectList=state.collectList.map(item=>{
            item.ischeck=state.allchioce
            return item
        })
    },
    delCollect(state,data){
        let ids=data.product_ids.split(',')
        let arr=[]
        state.collectList.map(item=>{
            ids.map(j=>{
                if(item.id!=j){
                    arr.unshift(item)
                }
                return j
            })
            return item
        })
        state.collectList=arr
    }
    
}
const actions = {
    async loadCollect({ commit }) {//加载收藏列表
        let obj={method:'get',url:'get_collection'}
        const result = await http(obj)
        if (result){
            commit('setList',result.data)
        } 
        return result
    },
    async addCollect({ dispatch }, params) {//加入收藏列表
        let obj={method:'put',url:'put_collection',data:params}
        const result = await http(obj)
        return result
    },
    async deleteCollect({ commit }, params) {//删除收藏列表-----------err
        let obj={method:'delete',url:'del_collection',data:params}
        const result = await http(obj)
        return result
    },
}
  
export default {
    namespaced: true,
    state,
    mutations,
    actions
}
  