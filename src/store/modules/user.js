import http from '@/modules/http'
import storage from '@/modules/storage'
import axios from 'axios'
import qs from 'qs'
import {Dialog ,Toast} from 'vant'
const state = {
    user:{},
    userInfo:null
}
const mutations = {
  setUser(state,data){
      state.user=data
      storage.set('session', data)
  },
  setUserInfo(state,data){
     state.userInfo=data
  },
  removeUser(state,data){
    state.token=null
    storage.remove('session')
  }
}
const actions = {
  async regist({ commit }, formData) {//注册用户
      try {
        const result = await axios.post(`${process.env.API_BASE_URL}/user_register`, qs.stringify(formData))
        await handleError(result.data)
        if(result.data.status===1){
          commit('setUserInfo',formData)
        }
        return result
      } catch (error) {
        if (error.response && error.response.data) {
          throw new Error(error.response.data.message)
        } else {
          throw error
        }
      }
  },
  async getweixinCode({commit},url) {//获取微信code的url
     const result = await axios.post(`${process.env.API_BASE_URL}/user_code`,qs.stringify(url))
     return result.data
  },
  async loginIn({dispatch, commit }, formData) {//登录用户
    try {
      const result = await axios.post(`${process.env.API_BASE_URL}/user_login`, qs.stringify(formData))
      await handleError(result.data)
      if (result) {
        let obj=Object.assign(formData,result.data.data)
        commit('setUser',obj)
      }
      return result.data
    } catch (error) {
      if (error.response && error.response.data) {
        throw new Error(error.response.data.message)
      } else {
        throw error
      }
    }
  },
  async loginOut({ commit }) {//退出登录用户
    const result = await request.post(`/user_out`)
    if (result)
    commit('removeUser')
    return result
  },
  async getUserInfo({ commit }) {//获取用户信息
    let obj={method:'get',url:'user_info'}
    const result = await http(obj)
    if(result) commit('setUserInfo',result.data)
    return result
  },
  async editUserInfo({ commit }, params) {//修改用户信息
    let obj={method:'post',url:'edit_user_info',data:params}
    const result = await http(obj)
    return result
  },
  async upLoadImg({ commit }, obj) {//上传文件
    let  formData = new FormData()
    formData.append('file', obj.file)
    let obj1={method:'upload',url:'upload',data:formData}
    const result = await http(obj1)
    return result
  },
  async uptatePwd({ commit }, params) {//修改用户密码
      let obj={method:'put',url:'update_password',data:params}
      const result = await http(obj)
      return result
  },
  async sendCode({ commit }, formData) {//发送验证码
    const result = await axios.post(`${process.env.API_BASE_URL}/sms`,qs.stringify(formData))
    handleError(result.data)
    return result.data
 },
}



async function handleError(data){
  if(data.status===0){
    await Dialog.alert({
      title: '错误提示',
      message: `${data.message}`
    })
  }else if(data.status===1){
    Toast(`${data.message}`)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
