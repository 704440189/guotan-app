import http from '@/modules/http'
import qs from 'qs'
import storage from '@/modules/storage'
const state = {
     isStart:0,//秒杀状态0：还未推出 1：未开始 2：开始
     unkillList:[],//未开始的秒杀列表
     killList:[],//正在进行的秒杀列表
     detail:{}//秒杀商品详情
  }
const mutations = {
    setUnList(state,data){
       state.unkillList=data
    },
    setList(state,data){
        state.killList=data
    },
    setDetail(state,data){
        state.detail=data
    }
}
const actions = {
    async getUnKill({ commit }) {//未开始的秒杀列表
        let obj={method:'get',url:'seckill_future'}
        const result = await http(obj)
        //const result = await request.get(`/seckill_future`)
        if (result) commit('setUnList',result.data)
        return result
    },
    async getKill({ commit }) {//正在进行的秒杀列表
        let obj={method:'get',url:'seckill_begin'}
        const result = await http(obj)
        //const result = await request.get('/seckill_begin')
        if (result) commit('setList',result.data)
        return result
    },
    async getKillDetail({ commit },params) {//秒杀详情
        let obj={method:'post',url:'seckill_detail',data:params}
        const result = await http(obj)
        //const result = await request.post(`/seckill_detail`,qs.stringify({id}))
        if (result) commit('setDetail', result.data)
        return result
    },
    async createKillOrder({ commit },params) {//秒杀下单
        let obj={method:'post',url:'seckill_create',data:params}
        const result = await http(obj)
        console.log(params)
        // const result = await request.post('/seckill_create')
        return result
    },
}
  
export default {
    namespaced: true,
    state,
    mutations,
    actions
}
  