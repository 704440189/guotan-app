import storage from '@/modules/storage'
const state = {
    searchHistory:storage.get('search')||[],
  }
  const mutations = {
    addHistory(state,data){//增加搜索历史数据
      state.searchHistory.unshift(data)
      state.searchHistory=state.searchHistory.slice(0,9)
      storage.set('search',state.searchHistory)
    },
    deleteHistory(state,data){//删除搜索数据
      state.searchHistory=[]
      storage.set('search',state.searchHistory)
    }
  }
  const actions = {
  }
  
  export default {
    namespaced: true,
    state,
    mutations,
    actions
  }
  