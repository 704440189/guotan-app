import http from '@/modules/http'
import qs from 'qs'
import storage from '@/modules/storage'
const state = {
    banner:[],
    hotList:null,
    otherList:null,
    detail:null
  }
const mutations = {
    setBanner(state,data){
       state.banner=data.images
    },
    setHot(state,data){
        state.hotList=data
    },
    setOther(state,data){
        state.otherList=data
    },
    setDetail(state,data){
        state.detail=data
    }
}
const actions = {
    async getBanner({ commit }) {//主页banner
        let obj={method:'post',url:'banner'}
        const result = await http(obj)
        if (result) commit('setBanner',result.data)
        return result
    },
    async getHot({ commit }) {//主页热卖
        let obj={method:'post',url:'hot',data:{offset:0,length:2}}
        const result = await http(obj)
        if (result) commit('setHot',result.data)
        return result
    },
    async getOther({ commit }) {//主页其他
        let obj={method:'get',url:'other'}
        const result = await http(obj)
        if (result)
          commit('setOther', result.data)
        return result
    },
    async getDetail({commit},params){//普通商品详情
        let obj={method:'post',url:'product_detail',data:params}
        const result = await http(obj)
        if (result) commit('setDetail', result.data)
        return result
    }
}
  
export default {
    namespaced: true,
    state,
    mutations,
    actions
}
  