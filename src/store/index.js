import Vue from 'vue'
import Vuex from 'vuex'

import storage from '@/modules/storage'

import user from './modules/user'
import home from './modules/home'
import kill from './modules/kill'
import mine from './modules/mine'
import search from './modules/search'
import cart from './modules/cart'
import collect from './modules/collect'
import address from './modules/address'
import order from './modules/order'

Vue.use(Vuex)

const syncStoreCart = store => {//vuex操作同步到storge
  store.subscribe((mutation, state) => {
    let user=state.user.user
   //console.log(user)
    storage.set(`cart`, state.cart.cartList)
  })
}
const syncStoreOrder = store => {//vuex操作同步到storge
  store.subscribe((mutation, state) => {
    storage.set(`comfirmOrder`, state.order.comfirmOrder)
  })
}

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  plugins: [syncStoreCart,syncStoreOrder],
  modules: {
    user,
    home,
    kill,
    mine,
    search,
    cart,
    collect,
    address,
    order
  }
})
