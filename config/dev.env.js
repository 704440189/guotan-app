'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_BASE_URL: '"http://neufit2.trycheers.com"'  //访问地址
  //API_BASE_URL: '"localhost:8080"'
})
